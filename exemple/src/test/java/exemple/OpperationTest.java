package exemple;

import static org.junit.Assert.*;

import org.junit.Test;

public class OpperationTest {

	@Test
	public void test() {
		//Arrange 
		int a = 5;
		int b = 5;
		
		//Act
		
		Cal cal = new Cal();
	    int res	= cal.somme(a,b);
		
		//assert
		assertEquals(10,res);
	}

	@Test
	public void SoustraTest() {
		//Arrange 
		int a = 5;
		int b = 5;
		
		//Act
		
		Cal cal = new Cal();
	    int res	= cal.soustra(a,b);
		
		//assert
		assertEquals(0,res);
	}
}
